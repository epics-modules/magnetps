magnetps
======

European Spallation Source ERIC Site-specific EPICS module : magnetps 

Dependencies 
============ 
iocshutils

Using the module 
================ 
In order to use the module declare a require statement for magnetps and device support for your power supply:
    
     require magnetps
     require caenfastps

Then load the power supply and the magnetps module (sample code below):

     iocshLoad("$(caenelfastps_DIR)/caenelfastps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.30.4.189, P=LEBT:, R=PwrC-PSCH-01:, MAVVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")
     iocshLoad("$(magnetps_DIR)/magnetps.iocsh", "P=LEBT:, R=PwrC-PSCH-01:, CONVERSIONTABLE=LEBT-010_BMD-CH-01")


Parameters (for power supply parameters please see module documentation for power supply device support module): 
- READCURPV: The PV that gets the current in the power supply device support (default Cur-R)
- CONVERSIONTABLE: The lookup table to perform the conversion between current and magnetic field (see below for supported tables)
- EGU: The engineering unit for the magnetic field. This is typically T for dipole magnets, T/m for quadropoles and T/m2 for sextupoles. (default T)

Supported magnetic field conversion tables
=======
For a list of the currently supported magnets see [magnetpcApp/Db](https://gitlab.esss.lu.se/epics-modules/magnetps/-/tree/master/magnetpsApp/Db)

Adding support for additional magnets
========
To add support for a new magnet create a break table for the conversion with the name "typeCur2Fld". Example:

     breaktable(typeCur2Fld) {
	    0 0
	    60000 0.0051
	    120000 0.0102
     }

Where the first column is the current output from the power supply in mA and the second column is the magnetic field strength in the engineering unit for your magnet type.

The conversion break table can be added to this device support module or provided locally, the former being recommended practise.

Building the module 
=================== 
This module should compile as a regular EPICS module. For instructions on how to build using E3 see the E3 documentation.

* Run `make` from the command line.
